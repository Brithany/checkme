var actualList;
var actualTask;

$(function () {
    loadLists();
});

function loadLists() {
    document.getElementById("myListUL").innerHTML = "";
    var endpoint = 'https://checkme-app.herokuapp.com/todolists';

    fetch(endpoint)
        .then(response => response.json())
        .then(lists => renderLists(lists)).catch(function () {
        alert("No és possible obtenir les llistes");
    });
}
function renderLists(lists) {
    console.log("renderLists()")
    for (i=0; i<lists.length; i++) {
        renderList(lists[i])
    }
}

function renderList(list) {
    var t = document.createTextNode(list.name);
    var div = document.createElement("div");
    div.classList.add("item")
    var li = document.createElement("li");
    div.id = list.name;

    li.appendChild(t);
    div.appendChild(li);

    var span = document.createElement("span");
    var spanEdit = document.createElement("span");
    var x = document.createTextNode("\u00D7");
    var edit = document.createTextNode("\u270E");
    span.className = "close";
    spanEdit.className = "edit";
    span.appendChild(x);
    spanEdit.appendChild(edit);
    div.appendChild(spanEdit);
    div.appendChild(span);

    document.getElementById("myListUL").appendChild(div);

    li.addEventListener('click', function () {
        toggleMenuTasks()
        actualList = list
        loadTasks(list);
        document.getElementById("list_name").innerHTML = list.name.toUpperCase() + ":";
    });
    spanEdit.addEventListener('click', function (){
        actualList = list;
        toggleEditList();
    });
    span.addEventListener('click', function () {
        deleteListAction(list)
    })
}

function inputEditList() {
    var inputValue = document.getElementById("edit_list_input").value;

    if (inputValue === '') {
        alert("Escriu el nou nom");
    } else {
        actualList.name = inputValue;
        editList(actualList);
    }

    document.getElementById("edit_list_input").value = "";
    var x = document.getElementById("list_edit_DIV");
    x.style.display = "none";
}

function editList(tList) {
    var endpoint = 'https://checkme-app.herokuapp.com/todolists';

    console.log(JSON.stringify(tList));
    fetch(endpoint,{
        method: 'PUT',
        body: JSON.stringify(tList),
        headers: {
            'Content-Type': 'application/json'
        }
    })
        .then(response => response.json())
        .then(loadLists())
        .catch(error => console.error("Error:", error));
}

function inputList() {
    var inputValue = document.getElementById("my_list_input").value;

    (inputValue === '') ? alert("Escriu una llista") : addList(new TList(inputValue))

    document.getElementById("my_list_input").value = "";
}

function addList(list) {
    var endpoint = 'https://checkme-app.herokuapp.com/todolists';

    console.log(JSON.stringify(list));
    fetch(endpoint, {
        method: 'POST',
        body: JSON.stringify(list),
        headers: {
            'Content-Type': 'application/json'
        }
    })
        .then(response => response.json())
        .then(newList => renderList(newList))
        .catch(error => console.error('Error: ', error))
}

function deleteListAction(list) {
    var endpoint ='https://checkme-app.herokuapp.com/todolists/'+list.idList;

    fetch(endpoint, {
        method: 'DELETE',
        body: JSON.stringify(list),
        headers: {
            'Content-Type' : 'application/json'
        }
    })
        .then(deleteList(list))
        .catch(error => console.error("Error: ", error))
}

function deleteList(list) {
    document.getElementById(list.name).remove();
}

function loadTasks(list) {
    document.getElementById("my_UL").innerHTML = "";
    var endpoint = 'https://checkme-app.herokuapp.com/todolists/'+list.idList+'/todoitems';

    fetch(endpoint)
        .then(response => response.json())
        .then(tasks => renderTasks(tasks))
}

function renderTasks(tasks) {
    console.log("renderTasks()")
    for (i=0;i <tasks.length; i++) {
        renderTask(tasks[i]);
    }
}

function renderTask(task) {
    var t = document.createTextNode(task.name);
    var div = document.createElement("div");
    div.classList.add("item");
    var li = document.createElement("li");
    div.id = "div_" + task.name;
    li.id = task.name;

    li.appendChild(t);
    div.appendChild(li);

    var span = document.createElement("span");
    var spanEdit = document.createElement("span")
    var x = document.createTextNode("\u00D7");
    var edit = document.createTextNode("\u270E");
    span.className = "close";
    spanEdit.className = "edit"
    span.appendChild(x);
    spanEdit.appendChild(edit)
    div.appendChild(spanEdit)
    div.appendChild(span);


    document.getElementById("my_UL").appendChild(div);
    if (task.done) {
        li.classList.add('checked');
    }

    li.addEventListener('click', function () {
        markTask(task);
    });
    spanEdit.addEventListener('click', function (){
        actualTask = task;
        toggleEditTask();
    })
    span.addEventListener('click', function () {
        deleteTaskAction(task);
    });
}

function inputEditTask(){
    var inputValue = document.getElementById("edit_task_input").value;

    if (inputValue === '') {
        alert("Escriu el nou nom");
    } else {
        actualTask.name = inputValue;
        editTask(actualTask);
    }

    document.getElementById("edit_task_input").value = "";
    var x = document.getElementById("task_edit_DIV");
    x.style.display = "none";
    loadTasks(actualList);

}

function markTask(task) {
    task.done = !task.done;

    if (task.done) {
        document.getElementById(task.name).classList.add('checked');
    } else {
        document.getElementById(task.name).classList.remove('checked');
    }

    editTask(task)
}


function editTask(task) {
    var endpoint = 'https://checkme-app.herokuapp.com/todolists/'+actualList.idList+'/todoitems';

    console.log(JSON.stringify(task));
    fetch(endpoint, {
        method: 'PUT',
        body: JSON.stringify(task),
        headers: {
            'Content-Type': 'application/json'
        }
    })
        .then(response => response.json())
        .catch(error => console.error("Error:", error));
}

function inputTask() {
    var inputValue = document.getElementById("my_input").value;

    (inputValue === '') ? alert("Escriu una tasca") : addTask(new Task(inputValue, false))

    document.getElementById("my_input").value = "";
}

function addTask(task) {
    var endpoint = 'https://checkme-app.herokuapp.com/todolists/'+actualList.idList+'/todoitems';

    console.log(JSON.stringify(task));
    fetch(endpoint, {
        method: 'POST',
        body: JSON.stringify(task),
        headers: {
            'Content-Type': 'application/json'
        }
    })
        .then(response => response.json())
        .then(newTask => renderTask(newTask))
        .catch(error => console.error('Error: ', error));
}

function deleteTaskAction(task) {
    var endpoint = 'https://checkme-app.herokuapp.com/todolists/'+actualList.idList+'/todoitems/'+task.idTask;

    fetch(endpoint, {
        method: 'DELETE',
        body: JSON.stringify(task),
        headers: {
            'Content-Type' : 'application/json'
        }
    })
        .then(deleteTask(task))
        .catch(error => console.error("Error: ", error))
}

function deleteTask(task) {
    document.getElementById("div_" + task.name).remove();

}

class TList {
    constructor(name) {
        this.name = name;
    }
}

class Task {
    constructor(name, done) {
        this.name = name;
        this.done = done;
    }
}




function toggleTaskAddBar(){
    var x = document.getElementById("my_DIV");
    var y = document.getElementById("plusTask");
    if (x.style.display === "flex"){
        x.style.display = "none"
        y.src = "img/plus.svg"
    }else{
        x.style.display = "flex"
        y.src = "img/minus.svg"
    }
}

function toggleAddBar(){
    var x = document.getElementById("my_list_DIV");
    var y = document.getElementById("plusList");
    if (x.style.display === "flex"){
        x.style.display = "none"
        y.src = "img/plus.svg"
    }else{
        x.style.display = "flex"
        y.src = "img/minus.svg"
    }
}

function toggleMenuLists(){
    var listDiv = document.getElementById("content__primary_list");
    var taskDiv = document.getElementById("content__secondary_list");
    var returnToList = document.getElementById("return_lists")
    if (listDiv.style.display === "none"){
        listDiv.style.display = "flex";
        taskDiv.style.display = "none";
        returnToList.style.display = "none";
    }
}

function toggleMenuTasks() {
    var taskDiv = document.getElementById("content__secondary_list");
    var listDiv = document.getElementById("content__primary_list");
    var returnToList = document.getElementById("return_lists")
    if (taskDiv.style.display === "none") {
        taskDiv.style.display = "flex";
        listDiv.style.display = "none";
        returnToList.style.display = "flex"
    }
}

function toggleEditList() {
    var x = document.getElementById("list_edit_DIV");
    if (x.style.display === "none") {
        x.style.display = "flex";
    } else {
        x.style.display = "none"
    }
}
function toggleEditTask() {
    var x = document.getElementById("task_edit_DIV");
    if (x.style.display === "none") {
        x.style.display = "flex";
    } else {
        x.style.display = "none"
    }
}